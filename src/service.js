/*
 * FlowCalc
 *
 * Copyright 2018, 2019 by Jaume Batlle Brugal <jaume.batlle@protonmail.com>
 *
 * This file is part of **FlowCalc**.
 *
 * FlowCalc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FlowCalc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlowCalc. If not, see <http://www.gnu.org/licenses/>.
 *
 * FlowCalc is a web application to calculate Water Flow and Foam application
 * for high demanding fires.

 * This is a preliminar version and values are hard-coded. It was made to
 * fullfill Barcelona's Fire Service, according to data given in its trainings,
 * and is meant to be a helping tool for commanders.
*/

// On installation, cache all static content of the site.
// Taken from Jake Archibald's "The Offline Cookbook"
// https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/

// Should it be defined globally? Maybe yes, but, will it be accessible from
// here? Options?

self.VERSION = 2;


/* This is a Single Page Application running entirely on the client side, so
 * it tries to load itself to cache first, and then work from there.
 * On subsequent runs, app is red from cache and then, it checks for network
 * availability to look for updates.
 */

// On install, it stores itself to cache.

self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open('flowcalc-v' + self.VERSION)
        .then(function(cache) {
            var app_files = [ './index.html'
                            , './main.css'
                            , './main.js'
                            , './favicon.png'
                            ];

            console.log("Storing cache for version:" + self.VERSION);
            return cache.addAll( app_files );
        })
    );
});


/* On activation, delete old caches */

/* Taken from Jake Archibald's "The Offline Cookbook"
https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/
*/

self.addEventListener('activate', function(event) {
    event.waitUntil(caches.keys()
        .then(function(cacheNames) {

            console.log ("Got cache.keys(). cacheNames is ", cacheNames);

            return Promise.all
                (cacheNames
                    .filter(function(cacheName) {
                        var vers;

                        console.log("Processing cache ", cacheName);

                        vers = parseFloat(cacheName.slice(10,15));

                        console.log("Found version to be ", vers);

                        if (vers < self.VERSION) {
                            console.log("    -> deleting");
                            return true;
                        }

                        console.log("    -> keeping");
                        return false;
                    })
                    .map(function(cacheName) {
                        return caches.delete(cacheName);
                    })
                );
        })
    );
});


/* On fetch, serves from cache */
// When should it update cache, now?

self.addEventListener('fetch', function (event) {
    event.respondWith(
        caches
        .match(event.request)
        .then(function(response) {
            return response || fetch(event.request);
        })
    );
});

