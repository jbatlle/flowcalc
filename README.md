## FlowCalc

Copyright 2018, 2019 by Jaume Batlle Brugal

This file is part of **FlowCalc**.

FlowCalc is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FlowCalc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with FlowCalc. If not, see <http://www.gnu.org/licenses/>.

FlowCalc is a web application to calculate Water Flow and Foam application for
high demanding fires.

In this first version of the program values are hard-coded. It was made to
fullfill Barcelona's Fire Service, according to data given in its trainings,
and is meant to be a helping tool for commanders.

### INSTALLATION

This is a web app. Just visit [FlowCalc's app page](https://jbatlle.gitlab.io/flowcalc) to run the
app. If your browser supports it, you'll be prompted to install the app (which
you can do so if you wish, or just run it from the browser).

###  USE

Using this program is quite straitforward, anyway, there are some indications
on how to do it:

You'll find a welcome screen with two big buttons. One will take you to the
part for water calculations, for building fires. The other one, will take you
to the foam flow calculations, meant for industry fires.

#### Water flow calculations

Set surface of the fire (in square meters). Use the sliders to indicate if
the fire is vented (has air intakes or not) and if it is "high intensity" or
not. This "intensity" is meant to be analised by expert eyes.

Under 'Results' you'll see an estimation of *critical water flow* needed to
stop this fire.

#### Foam calculations

This part is meant for industry or real big fires, where foam needs are hughe.
Set the substance that is burning, either a _polar_ substance or an
_hidrocarbon_.
Then, set the approximate surface, and if the fire is a "high intensity" one.
In the 'Results' part you'll see an estimation of water flow needed.
If you set a time, you'll have the amount of foam-forming agent needed in that
time.

#### Hard coded values used in calculcations

High demanding fires need a _Critical Flow_ of extinguishing agent for it to be
effective. The vaules used in here are extracted from a self-training course
taken at Barcelona's Fire Department in 2018. Data is as follows:

__Non-vented house fires__: Don't need high amounts of water, so this program
is not necessary for them, as a single water hydrant is enough to feed a
truck with a single hose line. The program gives a 1 litre/min per square
meter as a dummy value.

__Vented house fires__: Depending on whether there is a high intensity fire or
a low intensity one, calculated water flow is:

- _Low intensity_ : 5 litres/minute per square meter.
- _High intensity_: 10 litres/minute per square meter.

**NOTE:** We understand that any widespread fire, or one that affects more than
one level of the building is a high intensity fire.

__Foam-forming angent and water mix flow for *hydrocarbons*__:
There is a distinction according to area and fire intensity of the fire:

_Low intensity fire_ : 4 litres/minute per square meter.

_High intensity fire_:

- Surface under 400 square meters:  8 litres/min * m2.
- Surface above 400 square meters: 10 litres/min * m2.

__Foam-forming angent and water mix for *polar flammable liquids*:__
The same distinction is made as in hydrocarbon case:

_Low intensity fire_ : 8 litres/minute per square meter.
_High intensity fire_:

- Surface under 400 square meters: 16 litres/min * m2.
- Surface above 400 square meters: 20 litres/min * m2.

### BUILD

#### Get the Sources

This project is hosted at [GitLab](https://gitlab.com/jbatlle/flowcalc).

The recommended way of getting the source code is to clone it with git:

```bash
git clone https://gitlab.com/jbatlle/flowcalc.git
```

You are welcome to clone, fork and play with it if you like, as long as you
follow the guidelines of the AGPL-3.0+

#### Compile

Most of this program is written in the ELM programming language. Some parts of
it are HTML (manily a container for it) and it has some CSS styling, and a
'favicon' file.

In the src/ folder are the sources of all files. Keep them well indented and
with comments in to allow for debugging and documenting. The "production" part
is in the public/ directory. Here is where you can place all minified code
for production deployment.

To compile the Elm part, invoke the elm compiler in this way, from the
project's root directory (where the `elm.json` file is):

```bash
elm make src/main.elm --output=public/main.js --optimize
```

Then visit `public/index.html` in a browser.

More details and informatin on compiling elm programs can be found
at [elm-lang.org](https://elm-lang.org).
