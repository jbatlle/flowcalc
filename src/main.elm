module FlowCalc exposing (main)

{-| FlowCalc Copyright 2018 by Jaume Batlle Brugal
              <jaume.batlle@protonmail.com>

This file is part of FlowCalc.

  FlowCalc is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  FlowCalc is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with FlowCalc.  If not, see <http://www.gnu.org/licenses/>.

FlowCalc is an application to calculate the needs of water or foam by
Firefighting services in fires.
It is made to fulfill Barcelona's Fire Department (SPEIS Barcelona) needs in
calculations of water and foam in big fires. All reference values are hard
coded, and refer to values given in training received in-house.
-}

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onCheck, onInput)
import String exposing (fromInt)


main = Browser.element
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }


-- MODEL


type alias FoamData =
    { substance: FlammableSubstance
    , area: Int
    , intensity: FireIntensity
    , time : Int
    }


type alias WaterData =
    { vented : Bool
    , area: Int
    , intensity: FireIntensity
    }


type Model
    = NoneSelected
    | Foam FoamData
    | Water WaterData


type FlammableSubstance = Hidrocarbon | Polar


-- It is not a Bool to allow for a "Medium" intensity if needed.
type FireIntensity = Low | High


--INIT


init : () -> (Model, Cmd Msg)
init _ =
    ( NoneSelected, Cmd.none)


initFoamData : FoamData
initFoamData =
    FoamData Hidrocarbon 200 Low 15


initWaterData: WaterData
initWaterData =
    WaterData False 40 Low


-- VIEW


view: Model -> Html Msg
view model =
    case model of
        NoneSelected ->
            welcomeView model

        Water data ->
            waterView data

        Foam data ->
            foamView data


welcomeView: Model -> Html Msg
welcomeView model =
    div [ class "welcome" ]
        [ h1 [] [ text "Càlcul d'instal.lacions" ]
        , div [ class "centered" ]
            [ button [ class "bigbutton", onClick WaterPlease ]
                [ text "Aigua" ]
            , button [ class "bigbutton" , onClick FoamPlease ]
                [ text "Escumes" ]
            ]
        , copyNote
        ]


-- View for Water flow screen

waterView: WaterData -> Html Msg
waterView data =
    div []
        [ div []
            [ h1 [] [ text "Cabal crític H", sub [] [ text "2" ], text "O" ] ]
        , div []
            [ input
                [ id "check1"
                , class "slide-switch"
                , type_ "checkbox"
                , onClick ToggleVentedFire
                ] []
            , label [ for "check1" ] [ ]
            , ventedFireText data.vented
            ]
        , div []
            [ text "Superfície: "
            , button
                [ id "plus-button"
                , onClick (AddArea 10)
                ]
                [ text "+" ]
            , input
                [ type_ "text"
                , value (String.fromInt data.area)
                , onInput SetArea
                ] [ ]
            , button
                [ id "less-button"
                , onClick (AddArea -10)
                ] [ text "-" ]
            ]
        , div []
            [ input
                [ id "check2"
                , type_ "checkbox"
                , class "slide-switch"
                , onClick ToggleIntensity
                ] []
            , label [ for "check2" ] [ ]
            , highIntensityFireText data.intensity
            ]
        , div [ class "results" ]
            [ h2 [] [ text "Resultats" ]
            , waterFlow data
            ]
        , div [ class "centered" ] [ homeButton ]
        ]


ventedFireText : Bool -> Html Msg
ventedFireText isVented =
    let
        negate =
            if isVented then
                text ""
            else
                span [ class "intensify" ] [ text "no " ]

    in
        span []
            [ text "Foc "
            , negate
            , text "ventilat"
            ]


highIntensityFireText : FireIntensity -> Html Msg
highIntensityFireText intensity =
    let
        intensityText =
            if intensity == High then
                span [ class "intensify" ] [ text "alta " ]
            else
                text "baixa "

    in
        span []
            [ text "Incendi de "
            , intensityText
            , text "intensitat"
            ]

waterFlow : WaterData -> Html Msg
waterFlow data =
    let
        areaFactor =
            if not data.vented then
                1
            else if data.intensity == Low then
                5
            else
                10

    in
        span []
            [ text "Cabal crític: "
            , b [] [ text (String.fromInt (data.area * areaFactor)) ]
            , text " litres/min"
            ]


-- View for Foam Rate Screen

foamView: FoamData -> Html Msg
foamView data =
    div []
        [ div []
            [ h1 [] [ text "Càlcul d'escumes" ] ]
        , div []
            [ fieldset []
                [ text "Combustible: "
                , isHidrocarbon data
                    |>radiobutton "Hidrocarbur" (SetFlammable Hidrocarbon)
                , isHidrocarbon data
                    |> not
                    |> radiobutton "Polar" (SetFlammable Polar)
                ]
            ]
        , div []
            [ text "Superfície: "
            , button [ onClick (AddArea 50) ] [ text "+" ]
            , input
                [ type_ "text"
                , value (String.fromInt data.area)
                , onInput SetArea
                ] [ ]
            , button [ onClick (AddArea -50) ] [ text "-" ]
            ]
        , div []
            [ input
                [ id "check1"
                , type_ "checkbox"
                , class "slide-switch"
                , onClick ToggleIntensity
                ] []
            , label [ for "check1" ] [  ]
            , highIntensityFireText data.intensity
            ]
        , div []
            [ text "Temps: "
            , button [ onClick (AddTime 15) ] [ text "+" ]
            , " minuts"
                |> (++) (String.fromInt data.time)
                |> text
            , button [ onClick (AddTime -15)] [ text "-" ]
            ]
        , div [ class "results" ]
            [ h2 [] [ text "Resultats" ]
            , foamFlowMsg data
            , foamQuantityMsg data
            ]
        , div [ class "centered" ] [ homeButton ]
        ]


foamQuantityMsg : FoamData -> Html msg
foamQuantityMsg data =
    div []
        [ text "Quantitat d'escumògen: "
        , " litres"
            |> ((++)
                <| String.fromInt
                <| foamQuantity
                <| data
                )
            |> text
        ]


foamQuantity : FoamData -> Int
foamQuantity data =
    let
        dosification = 0.03

    in
        dosification * toFloat (foamMixFlowrate data) * toFloat data.time
        |> round


foamFlowMsg : FoamData -> Html Msg
foamFlowMsg data =
    div []
        [ text "Cabal H"
        , sub [] [ text "2" ]
        , text "O :"
        , text (String.fromInt (foamMixFlowrate data))
        , text " l/min"
        ]


-- Water and foam mix flowrate = Application rate * area

foamMixFlowrate : FoamData -> Int
foamMixFlowrate data =
    let
        factor =
            case data.substance of
                Hidrocarbon ->
                    hidrocarbonFactor data

                Polar ->
                    polarFactor data

    in
        factor * data.area


hidrocarbonFactor : FoamData -> Int
hidrocarbonFactor data =
    if data.area <= 400 then
        4
    else if data.intensity == Low then
        8
    else
        10


polarFactor : FoamData -> Int
polarFactor data =
    if data.area <= 400 then
        6
    else if data.intensity == Low then
        16
    else
        20


radiobutton : String -> msg -> Bool -> Html msg
radiobutton value msg sel =
    label []
        [ input
            [ type_ "radio"
            , name "substance"
            , onClick msg
            , checked sel
            ] []
        , text value
        ]


isHidrocarbon: FoamData -> Bool
isHidrocarbon data =
    if data.substance == Hidrocarbon then
        True
    else
        False


homeButton : Html Msg
homeButton =
    button [ class "bigbutton", onClick GoHome ] [ text "Tornar" ]


copyNote : Html Msg
copyNote =
    div [ class "copyright" ]
        [ text "Copyright 2018, 2019 by Jaume Batlle Brugal"
        , br [] []
        , text "Sota llicència "
        , a [ href "http://www.gnu.org/licenses/agpl-3.0-standalone.html"
            , target "_blank"
            ]
            [ text "Affero GPL 3.0+" ]
        , text ", "
        , a [ href "https://gitlab.com/jbatlle/flowcalc" ] [ text "Codi font" ]
        ]


-- UPDATE


type Msg
    = FoamPlease
    | WaterPlease
    | GoHome
    | ToggleVentedFire
    | AddArea Int
    | SetArea String
    | ToggleIntensity
    | SetFlammable FlammableSubstance
    | AddTime Int


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        -- Page selection
        GoHome ->
            ( NoneSelected, Cmd.none )

        WaterPlease ->
            ( Water initWaterData, Cmd.none )

        FoamPlease ->
            ( Foam initFoamData, Cmd.none )

        -- Value changes
        _ ->
            case model of
                Water data ->
                    updateWater msg data

                Foam data ->
                    updateFoam msg data

                _ ->
                    ( model, Cmd.none )


updateWater : Msg -> WaterData -> ( Model, Cmd Msg )
updateWater msg data =
    case msg of
        ToggleVentedFire ->
            ( Water {data | vented = not data.vented }, Cmd.none )

        ToggleIntensity ->
            let
               newIntensity =
                   if data.intensity == High then
                       Low
                   else
                       High
            in
               ( Water { data | intensity = newIntensity }, Cmd.none )

        AddArea n ->
            ( Water { data | area = addGEZero data.area n }, Cmd.none )

        SetArea str ->
            ( Water
               { data | area =
                   str
                   |> String.toInt
                   |> Maybe.withDefault 0
                   |> neverNegative
               }
            , Cmd.none
            )

        _ -> -- should be unreachable
            ( Water data, Cmd.none )


updateFoam : Msg -> FoamData -> ( Model, Cmd Msg )
updateFoam msg data =
    case msg of
        ToggleIntensity ->
            let
               newIntensity =
                   if data.intensity == High then
                       Low
                   else
                       High
            in
               ( Foam { data | intensity = newIntensity }, Cmd.none )

        AddArea n ->
            ( Foam { data | area = addGEZero data.area n }, Cmd.none )

        SetArea str ->
            ( Foam
               { data | area =
                   str
                   |> String.toInt
                   |> Maybe.withDefault 0
                   |> neverNegative
               }
            , Cmd.none
            )

        SetFlammable newSubstance ->
            ( Foam
                { data | substance = newSubstance }
            , Cmd.none
            )

        AddTime t ->
            ( Foam
                { data | time = addGEZero data.time t }
            , Cmd.none
            )

        _ -> -- unreachable branch; caller handles rest of cases
            ( Foam data, Cmd.none )


addGEZero: Int -> Int -> Int
addGEZero m n =
    let
        result = m + n
    in
        neverNegative result


neverNegative: Int -> Int
neverNegative n =
    if n < 0 then
        0
    else
        n

-- SUBSCRIPTIONS


subscriptions model =
  Sub.none
